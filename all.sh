#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
PATH=$prefix_dir/bin:$PATH
source $prefix_dir/lib/dakota/platform.sh
threads=$(getconf _NPROCESSORS_ONLN)
threads_per_core=1 # on linux it reports number of cores (not number of threads)
if [[ $host_system_name == 'Darwin' ]]; then
  threads_per_core=2 # on darwin it reports number of threads (not number of cores)
fi
jobs=$(( threads / threads_per_core ))
SECONDS=0
notimings=1 make -k -s -j $jobs -f $source_dir/z/intmd/build.mk $@ || true
duration=$SECONDS
echo "duration: $(($duration / 60))m$(($duration % 60))s"
