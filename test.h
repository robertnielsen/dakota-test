inline FUNC get_klass_chain(object_t kls, char_t* buf, ssize_t buf_len) -> char_t* { // recursive
  object_t superkls = superklass_of(kls);
  if (superkls != null)
    get_klass_chain(superkls, buf, buf_len); // recursive
  str_t kls_name = name_of(kls);
  strcat(buf, "/");
  strcat(buf, kls_name);
  return buf;
}
inline FUNC dkt_get_klass_chain(object_t kls, char_t* buf, ssize_t buf_len) -> char_t* {
  buf[0] = NUL;
  return get_klass_chain(kls, buf, buf_len);
}
