source_dir ?= ${HOME}/dakota/test # fixfix
prefix_dir := $(dir ${source_dir})

include ${prefix_dir}/lib/dakota/platform.mk

system := $(shell echo ${host_system_name} | tr '[:upper:]' '[:lower:]')

.PHONY: \
 all\
 clean\
 clean-check\
 check\
 check-report\
#

all:
	./all.sh

clean:
	./clean.sh

check-clean:
	find z/intmd -name check.pass -exec rm -f {} \;
	find z/intmd -name check.fail -exec rm -f {} \;

check:
	./check-recipe.sh
	${MAKE} -${MAKEFLAGS} check-report

check-report:
	./bin/check-report.sh should-fail should-pass/fail should-pass/pass
	@grep -h summary: check-history/${system}/should-fail/last.txt check-history/${system}/should-pass/fail/last.txt check-history/${system}/should-pass/pass/last.txt
