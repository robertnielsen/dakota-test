#!/usr/bin/env bash

SECONDS=0
threads=$(getconf _NPROCESSORS_ONLN)
# threads_per_core should be 1 on linux
threads_per_core=2
jobs=$(( threads / threads_per_core ))
make --keep-going --jobs $jobs --file z/intmd/check.mk
duration=$SECONDS
echo "time: $(($duration / 60))m$(($duration % 60))s"
