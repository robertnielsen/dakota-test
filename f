#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
SECONDS=0
make -k $@ clean
make -k $@ all
make -k $@ check
duration=$SECONDS
echo "time: $(($duration / 60))m$(($duration % 60))s"
