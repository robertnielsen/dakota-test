#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
lib_prefix=lib
for DIR in should-pass/pass should-pass/fail should-fail; do
	rm -fr $DIR/*/{dakota,${lib_prefix}{l1,l2,l3}}.project
	rm -fr $DIR/*/{exe,${lib_prefix}{l1,l2,l3}.{so,dylib}}
	bin/run-each-script-in-dir.sh $$DIR/*/clean.sh
	rm -fr $DIR/*/check.{fail,pass}
	rm -fr $DIR/*/dkt
done
./mk-clean
