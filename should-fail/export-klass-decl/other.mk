all: ${target}

${target}: ${prereq} libl1${lib_suffix}
	${DAKOTA} ${DAKOTAFLAGS} ${EXTRA_DAKOTAFLAGS} --include-directory=. --output $@ $^

libl1${lib_suffix}: file-1.dk file-2.dk module-libl1.dk
	${DAKOTA} --shared ${DAKOTAFLAGS} ${EXTRA_DAKOTAFLAGS} --include-directory=. --output $@ $^

check:
	./${target}

clean:
	${RM} ${RMFLAGS} build
	${RM} ${RMFLAGS} ./${target} file-{1,2}.o libl1${lib_suffix} *~
