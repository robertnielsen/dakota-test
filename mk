#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
source $prefix_dir/lib/dakota/platform.sh
threads=$(getconf _NPROCESSORS_ONLN)
threads_per_core=1 # on linux it reports number of cores (not number of threads)
if [[ $host_system_name == 'Darwin' ]]; then
  threads_per_core=2 # on darwin it reports number of threads (not number of cores)
fi
jobs=$(( threads / threads_per_core ))
dirs="."
if [[ $# > 0 ]]; then
  dirs=$(echo $@ | sort)
fi

for dir in $dirs; do
  echo "# $dir/exe"
  pushd $dir > /dev/null
  cwd=$(pwd)
  popd > /dev/null
  target=
  if [[ $cwd != $source_dir ]]; then
    target=$cwd/exe
  fi
  make -s -j $jobs -f $source_dir/z/intmd/build.mk $target
done
