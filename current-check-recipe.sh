#!/usr/bin/env bash

source_dir=$1
current_source_dir=$2
current_build_dir=$3

timeout=3
rm -f $current_build_dir/check.pass
touch $current_build_dir/check.fail

pushd $current_source_dir > /dev/null
if [[ -e ./check.sh ]]; then
  ./check.sh $source_dir/bin/run-with-timeout $timeout ./exe > /dev/null 2>&1
else
  $source_dir/bin/run-with-timeout $timeout ./exe > /dev/null 2>&1
fi
if [[ $? == 0 ]]; then
  mv $current_build_dir/check.fail $current_build_dir/check.pass
  echo pass: $current_source_dir
else
  echo fail: $current_source_dir
fi
popd > /dev/null
