#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
source $prefix_dir/lib/dakota/platform.sh
system=$(echo $host_system_name | tr '[:upper:]' '[:lower:]')
mkdir -p check-history/$system/should-fail
mkdir -p check-history/$system/should-pass/{fail,pass}

num=$(printf "%0.5i" $$)

for dir in $@; do
  path=check-history/$system/$dir/$num.txt
  DIR=$dir bin/check-report.pl --output $path $dir/*/
  rm -f check-history/$system/$dir/last.txt
  pushd check-history/$system/$dir; ln -s $num.txt last.txt; popd
  echo "$path" >> check-history/$system/$dir.txt
  git add $path
 #git commit check-history/$system/$dir.txt $path -m "check: added $path."
  cat $path
done
