#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
source $prefix_dir/lib/dakota/platform.sh
system=$(echo $host_system_name | tr '[:upper:]' '[:lower:]')

skip=0
for path in $(cat check-history/$system/should-pass/pass.txt); do
    if [[ 0 != $skip ]]; then
        skip=0
    else
        touch $path
        sleep 2
        skip=1
    fi
done
