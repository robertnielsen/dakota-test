#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;

use Getopt::Long;
$Getopt::Long::ignorecase = 0;

my $nl = "\n";

my $opts = {};
&GetOptions($opts, 'output=s');

my $verbose = 1;

my $num_pass = 0;
my $num_failed_build = 0;
my $num_failed_run = 0;
my $failed_build = [];
my $failed_run = [];

sub _dirname {
  my ($path) = @_;
  $path =~ s#(/[^/]+$)##;
  return $path;
}

# require that $rel_src_dir is relative to $source_dir
foreach my $rel_src_dir (@ARGV) {
  print STDERR "ERROR: missing $rel_src_dir/exe.dk\n" if ! -e "$rel_src_dir/exe.dk";
  $rel_src_dir =~ s=/$==; # remove trailing slash
  my $name = 'exe';
  my $rel_exe = "$rel_src_dir/$name";
  if (! -e $rel_exe) {
    print "fail  $rel_exe\n" if $verbose;
    push @$failed_build, $rel_exe;
    $num_failed_build++;
  } else {
    my $rel_intmd = 'z/intmd';
    my $rel_dst_dir = &_dirname("$rel_intmd/$rel_exe");
    $ENV{'DK_DIR'} = $rel_dst_dir;

    if (-e "$rel_dst_dir/check.pass") {
      print "pass  $rel_exe\n" if $verbose;
      $num_pass++;
    } else {
      print "fail  $rel_exe\n" if $verbose;
      push @$failed_run, $rel_exe;
      $num_failed_run++;
    }
  }
}

my $num_failed = $num_failed_build + $num_failed_run;
my $total = $num_pass + $num_failed;

if ($$opts{'output'}) {
  my $file = $$opts{'output'};
  open STDOUT, ">$file" or die __FILE__, ":", __LINE__, ": ERROR: $file: $!\n";
}

if (0 != $num_failed_build) {
  print "# build ($num_failed_build failure(s))\n";
  print "build-fail-exe-files :=\\\n";
  my $exe;
  foreach $exe (@$failed_build) {
    print " $exe\\\n";
  }
  print "\n";
}

if (0 != $num_failed_run) {
  print "# run ($num_failed_run failure(s))\n";
  print "run-fail-exe-files :=\\\n";
  my $exe;
  foreach $exe (@$failed_run) {
    print " $exe\\\n";
  }
  print "\n";
}
my $pad = ' ' x (17 - length($ENV{'DIR'}));
my $summary = sprintf("# summary: $ENV{'DIR'}/*:" . $pad . "pass/total = %3i/%3i (%3i + %3i = %3i failure(s))\n",
                      $num_pass,
                      $total,
                      $num_failed_build,
                      $num_failed_run,
                      $num_failed);
print $summary;
