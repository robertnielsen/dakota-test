#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

source bin/common.sh

dk_paths=$(paths-from-pattern "$2/*/exe.dk")

for exe_src_path in $dk_paths; do
    dir=$(dirname $exe_src_path)
    echo "$dir/$1"
done
