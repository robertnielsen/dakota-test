#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
bin/quick-check.sh should-pass/pass/{sets,counted-sets,tables}
