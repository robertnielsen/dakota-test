#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
source $prefix_dir/lib/dakota/platform.sh
system=$(echo $host_system_name | tr '[:upper:]' '[:lower:]')

dirs=$@

if [[ 0 -eq $# ]]; then
    dirs="should-pass/pass"
fi

for dir in $dirs; do
  grep "summary: " $(ls -r -t check-history/$system/$dir/*.txt | grep -v last.txt)
done
