#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
source_dir=${source_dir:-$HOME/dakota/test} # fixfix
prefix_dir=$(dirname $source_dir)
export PREFIX_PATH=$HOME:$prefix_dir
export PATH=$HOME/bin:$prefix_dir/bin:$PATH
#export DKT_BUILD_PASS_FAIL=1
./gen-mk.pl
