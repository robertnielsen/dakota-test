#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

files="libdakota.perl libdakota.perl.dump"
rm -f $files
