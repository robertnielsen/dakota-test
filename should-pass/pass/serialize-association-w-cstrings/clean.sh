#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail

files="out.pl out.dot"
rm -f $files
