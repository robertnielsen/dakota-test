.PHONY: single single1 single2 single3 single4

single:
	make clean single1
	make clean single2
	make clean single3
	make clean single4

single1: exe.dk | project
	${DAKOTA} --compile --project project --output ${BUILDDIR}/exe.dk.o exe.dk
	${DAKOTA}           --project project --output exe ${BUILDDIR}/exe.dk.o
	find build -type f | sort

single2: exe.dk | project
	${DAKOTA} --compile --project project --output ${BUILDDIR}/exe.o exe.dk
	${DAKOTA}           --project project --output exe ${BUILDDIR}/exe.o
	find build -type f | sort

single3: exe.dk | project
	${DAKOTA} --compile --project project --output ${BUILDDIR}/exe.cc.o exe.dk
	${DAKOTA}           --project project --output exe ${BUILDDIR}/exe.cc.o
	find build -type f | sort

single4: exe.dk | project
	${DAKOTA} --compile --project project exe.dk
	${DAKOTA}           --project project --output exe
	find build -type f | sort
