include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl1.mk libl1.build)
include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl2.mk libl2.build)

include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single ls

single: exe.dk | project libl1.project libl2.project
	${DAKOTA} --compile --project libl1.project libl1.dk
	${DAKOTA} --compile --project libl1.project module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}
	${DAKOTA} --compile --project libl2.project libl2.dk
	${DAKOTA} --compile --project libl2.project module-libl2.dk
	${DAKOTA} --shared  --project libl2.project --output libl2${lib_suffix}
	${DAKOTA} --compile --project project exe.dk
	${DAKOTA}           --project project --output exe

ls:
	find build | grep \\.o | sort
