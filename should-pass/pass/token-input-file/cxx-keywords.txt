auto   const     double  float  int       short   struct   unsigned
break  continue  else    for    long      signed  switch   void
case   default   enum    goto   register  sizeof  typedef  volatile
char   do        extern  if     return    static  union    while

asm         dynamic-cast  namespace  reinterpret-cast  try
bool        explicit      new        static-cast       typeid
catch       false         operator   template          typename
class       friend        private    this              using
const-cast  inline        public     throw             virtual
delete      mutable       protected  true              wchar-t

and      bitand   compl   not-eq   or-eq   xor-eq
and-eq   bitor    not     or       xor
