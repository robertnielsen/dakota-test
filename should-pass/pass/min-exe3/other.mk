include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl1.mk libl1.build)
include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl2.mk libl2.build)
include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl3.mk libl3.build)

include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single libl1 ls

single: exe.dk | project libl1.project libl2.project libl3.project
	${DAKOTA} --compile --project libl1.project libl1.dk
	${DAKOTA} --compile --project libl1.project module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}
	${DAKOTA} --compile --project libl2.project libl2.dk
	${DAKOTA} --compile --project libl2.project module-libl2.dk
	${DAKOTA} --shared  --project libl2.project --output libl2${lib_suffix}
	${DAKOTA} --compile --project libl3.project libl3.dk
	${DAKOTA} --compile --project libl3.project module-libl3.dk
	${DAKOTA} --shared  --project libl3.project --output libl3${lib_suffix}
	${DAKOTA} --compile --project project exe.dk
	${DAKOTA}           --project project --output exe

libl1: | libl1.project
	${DAKOTA} --compile --project libl1.project libl1.dk
	${DAKOTA} --compile --project libl1.project module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}

libl1-2: | libl1.project
	${DAKOTA} --compile --project libl1.project libl1.dk module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}

ls:
	find build | grep \\.o | sort
