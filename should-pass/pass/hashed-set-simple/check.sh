#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
DK_ENABLE_TRACE=1 ../../../bin/run-with-timeout 3 ./exe
