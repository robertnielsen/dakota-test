#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

exe_inc=../../../z/intmd/should-pass/pass/nested-switches-containing-ints-then-strings/exe/exe.inc 
line=$(grep -n 'dkt_hash_switch' $exe_inc /dev/null | grep int1)
if [[ 0 == $? ]]; then
    echo $line: switch expression should not have been rewritten.
fi
