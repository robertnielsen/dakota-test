include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/libl1.mk libl1.build)

include $(shell /usr/local/bin/dakota-build2mk --output ${BUILDDIR}/exe.mk dakota.build)

.PHONY: single ls

single: exe.dk | project libl1.project
	${DAKOTA} --compile --project libl1.project libl1.dk
	${DAKOTA} --compile --project libl1.project module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}
	${DAKOTA} --compile --project project  exe.dk
	${DAKOTA}           --project project --output exe

libl1: | libl1.project
	${DAKOTA} --compile --project libl1.project libl1.dk
	${DAKOTA} --compile --project libl1.project module-libl1.dk
	${DAKOTA} --shared  --project libl1.project --output libl1${lib_suffix}

ls:
	find build | grep \\.o | sort
