#!/usr/bin/perl -w
# -*- mode: cperl -*-
# -*- cperl-close-paren-offset: -2 -*-
# -*- cperl-continued-statement-offset: 2 -*-
# -*- cperl-indent-level: 2 -*-
# -*- cperl-indent-parens-as-block: t -*-
# -*- cperl-tab-always-indent: t -*-

use strict;
use warnings;

use Cwd;
use File::Basename;
use File::Spec;
use Data::Dumper;
$Data::Dumper::Terse =    1;
$Data::Dumper::Purity =   1;
$Data::Dumper::Sortkeys = 1;
$Data::Dumper::Indent =   1; # default = 2

my $nl = "\n";

sub path_split {
  my ($path) = @_;
  my $parts = [split /\//, $path];
  my $name = pop @$parts;
  my $dir = join '/', @$parts;
  $dir = '.' if $dir eq '';
  return ($dir, $name);
}
sub ds {
  my ($paths) = @_;
  my $data = {};
  foreach my $path (@$paths) {
    my ($dir, $name) = &path_split($path);
    if (! exists $$data{$dir}) {
      $$data{$dir} = {};
    }
    $$data{$dir}{$name} = 1;
  }
  return $data;
}
my $tbl = {
  'exe0' => [ 'exe.yaml' ],
  'exe1' => [ 'exe.yaml', 'libl1.yaml' ],
  'exe2' => [ 'exe.yaml', 'libl1.yaml', 'libl2.yaml' ],
  'exe3' => [ 'exe.yaml', 'libl1.yaml', 'libl2.yaml', 'libl3.yaml' ],
};
sub lines {
  my ($dir, $exen) = @_;
  `cp templates/$exen/*.yaml $dir`;
  die if $?;
  my $yamls = [map { "$dir/$_" } @{$$tbl{$exen}}];
  my $files = `dakota-make --var=lib_dir=$dir @$yamls`; # assume $dir is a abs path
  die if $?;
  $files = [split("\n", $files)];
  my $current_build_mk_path = &dirname($$files[0]) . '/build.mk';
  my $lines = join('', map { "include $_\n" } @$files);
  open(my $fh, '>', $current_build_mk_path) or die;
  print $fh $lines;
  close($fh);
  return $lines;
}
sub check {
  my ($source_dir, $current_source_dir) = @_;
  # assume $current_source_dir is a abs path
  my $file = `dakota-make --path-only --var=lib_dir=$current_source_dir $current_source_dir/exe.yaml`;
  die if $?;
  chomp $file;
  $file =~ s/exe\.mk$/check.mk/;
  my $current_intmd_dir = &dirname($file);
  my $line = "include $file\n";
  my $rel_current_intmd_dir = File::Spec->abs2rel($current_intmd_dir, $source_dir);
  my $recipe =
    ".PHONY : all" . $nl .
    $nl .
    "all : $current_intmd_dir/check.pass" . $nl .
    $nl .
    "$rel_current_intmd_dir/check.pass : $current_intmd_dir/check.pass" . $nl .
    $nl .
    "$current_intmd_dir/check.pass :" . $nl .
    "\t\@$source_dir/current-check-recipe.sh $source_dir $current_source_dir $current_intmd_dir" . $nl;
  open(my $fh, '>', "$current_intmd_dir/check.mk") or die;
  print $fh $recipe;
  close($fh);
  return $line;
}
sub gen {
  my ($source_dir, $data) = @_;
  my $dirs = [keys %$data];
  my $lines = '';
  my $check_lines = '';
  foreach my $dir (sort @$dirs) {
    print $dir . $nl;
    my $names = $$data{$dir};

    if (0) {
    } elsif ($$names{'libl3.yaml'}) {
      $lines .= &lines($dir, 'exe3');
    } elsif ($$names{'libl2.yaml'}) {
      $lines .= &lines($dir, 'exe2');
    } elsif ($$names{'libl1.yaml'}) {
      $lines .= &lines($dir, 'exe1');
    } elsif ($$names{'exe.yaml'}) {
      $lines .= &lines($dir, 'exe0');
    } else {
      print "warning: $dir contains no applicable *.yaml files" . $nl;
    }
    $check_lines .= &check($source_dir, $dir);
  }
  my $rel_build_dir = 'z/build';
  my $rel_intmd_dir = &dirname($rel_build_dir) . '/intmd';
  open(my $fh1, '>', "$rel_intmd_dir/build.mk") or die;
  print $fh1 $lines;
  close($fh1);
  open(my $fh2, '>', "$rel_intmd_dir/check.mk") or die;
  print $fh2 $check_lines;
  close($fh2);
}
sub start {
  my ($argv) = @_;
  my $rel_paths = [
    glob("should-pass/pass/*/*.yaml"),
    glob("should-pass/fail/*/*.yaml"),
    glob("should-fail/*/*.yaml")
  ];
  my $source_dir = &cwd();
  my $paths = [map { $source_dir . '/' . $_ } @$rel_paths];
  my $data = &ds($paths);
  &gen($source_dir, $data);
}
&start(\@ARGV);
