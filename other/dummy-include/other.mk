all: ${target}

${target}: ${prereq}
	clang --all-warnings --debug=3 --output $@ -ldl $^

check:
	./${target} ${prefix}/lib/libdakota${lib_suffix} ${prefix}/lib/libdakota${lib_suffix}

clean:
	${RM} ${RMFLAGS} ./${target} *~
