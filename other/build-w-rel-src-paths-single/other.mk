.PHONY: all check clean

builddir := dkt
bindir :=   bin

all: | dakota.project
	dakota --project dakota.project --compile --output tmp/d1/f1.dk.o       d1/f1.dk
	dakota --project dakota.project --compile --output tmp/d1/d2/f2.dk.o    d1/d2/f2.dk
	dakota --project dakota.project --compile --output tmp/d1/d2/d3/f3.dk.o d1/d2/d3/f3.dk
	dakota --project dakota.project --output bin/exe tmp/d1/f1.dk.o tmp/d1/d2/f2.dk.o tmp/d1/d2/d3/f3.dk.o
	if [[ -e ${builddir} ]]; then find ${builddir} -type f | sort; fi
	if [[ -e ${bindir}   ]]; then find ${bindir}   -type f | sort; fi
	if [[ -e tmp         ]]; then find tmp         -type f | sort; fi

check:
	${bindir}/exe

clean:
	rm -rf ${builddir} ${bindir} dakota.project tmp
