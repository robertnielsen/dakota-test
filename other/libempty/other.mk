.PHONY:\
 all\
 check\
 clean\

all:
	cat /dev/null > libempty.${cc_ext}
	clang++ -std=c++11 --shared -fPIC --output ./libempty${lib_suffix} libempty.${cc_ext}

check: all

clean:
	${RM} ${RMFLAGS} libempty${lib_suffix} libempty.${cc_ext}
